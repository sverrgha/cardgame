package stud.ntnu.idatt2003.cardgame.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

  @Test
  void testDealHandSize() {
    DeckOfCards deck = new DeckOfCards();
    List<PlayingCard> hand = deck.dealHand(5);
    assertEquals(5, hand.size());
  }

  @Test
  void testDealHandMaxFace() {
    DeckOfCards deck = new DeckOfCards();
    for (int i = 0; i < 100; i++) {
      List<PlayingCard> hand = deck.dealHand(5);
      assertTrue(hand.stream().allMatch(card -> card.getFace() <= 13));
    }
  }

  @Test
  void testDealHandMinFace() {
    DeckOfCards deck = new DeckOfCards();
    for (int i = 0; i < 100; i++) {
      List<PlayingCard> hand = deck.dealHand(5);
      assertTrue(hand.stream().allMatch(card -> card.getFace() >= 1));
    }
  }
}