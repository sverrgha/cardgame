package stud.ntnu.idatt2003.cardgame.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {
  static PlayingCard card;
  @BeforeAll
    static void setUp() {
    card = new PlayingCard('H', 12);
    }
  @Test
  void getAsString() {
    assertEquals("H12", card.getAsString());
  }

  @Test
  void getSuit() {
    assertEquals('H', card.getSuit());
  }

  @Test
  void getFace() {
    assertEquals(12, card.getFace());
  }

}