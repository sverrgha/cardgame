package stud.ntnu.idatt2003.cardgame.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {
  private static HandOfCards hand;
  private static List<PlayingCard> defaultCards;

  @BeforeAll
  static void setUp() {
    DeckOfCards deck = new DeckOfCards();
    defaultCards = new ArrayList<>();
    defaultCards.add(new PlayingCard('S', 1));
    defaultCards.add(new PlayingCard('S', 6));
    defaultCards.add(new PlayingCard('S', 5));
    defaultCards.add(new PlayingCard('S', 4));
    defaultCards.add(new PlayingCard('S', 12));
    hand = new HandOfCards(defaultCards);
  }

  @Test
  void getSumOfFaces() {
    assertEquals(28, hand.getSumOfFaces(hand));
  }

  @Test
  void hasFlushTrue() {
    assertTrue(hand.hasFlush(hand));
  }

  @Test
  void hasFlushFalse() {
    List<PlayingCard> cards = new ArrayList<>();
    cards.add(new PlayingCard('S', 1));
    cards.add(new PlayingCard('H', 6));
    cards.add(new PlayingCard('S', 5));
    cards.add(new PlayingCard('S', 4));
    cards.add(new PlayingCard('S', 12));
    HandOfCards hand = new HandOfCards(cards);
    assertFalse(hand.hasFlush(hand));
  }

  @Test
  void hasQueenOfSpadesTrue() {
    assertTrue(hand.hasQueenOfSpades(hand));
  }

  @Test
  void hasQueenOfSpadesFalse() {
    List<PlayingCard> cards = new ArrayList<>();
    cards.add(new PlayingCard('S', 1));
    cards.add(new PlayingCard('S', 6));
    cards.add(new PlayingCard('S', 5));
    cards.add(new PlayingCard('S', 4));
    cards.add(new PlayingCard('S', 11));
    HandOfCards hand = new HandOfCards(cards);
    assertFalse(hand.hasQueenOfSpades(hand));
  }

  @Test
  void getCardsOfHeartAsStringWithHearts() {
    List<PlayingCard> cards = new ArrayList<>();
    cards.add(new PlayingCard('S', 1));
    cards.add(new PlayingCard('H', 6));
    cards.add(new PlayingCard('H', 5));
    cards.add(new PlayingCard('S', 4));
    cards.add(new PlayingCard('S', 12));
    HandOfCards hand = new HandOfCards(cards);
    assertEquals(" H6 H5", hand.getCardsOfHeartAsString(hand));
  }
  @Test
    void getCardsOfHeartAsStringWithoutHearts() {
      assertEquals("No hearts", hand.getCardsOfHeartAsString(hand));
    }

  @Test
  void getHand() {
    assertEquals(defaultCards, hand.getHand());
  }
}