module stud.ntnu.idatt2003.cardgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens stud.ntnu.idatt2003.cardgame to javafx.fxml;
    exports stud.ntnu.idatt2003.cardgame;
  exports stud.ntnu.idatt2003.cardgame.model;
  opens stud.ntnu.idatt2003.cardgame.model to javafx.fxml;
  exports stud.ntnu.idatt2003.cardgame.view;
  opens stud.ntnu.idatt2003.cardgame.view to javafx.fxml;
  exports stud.ntnu.idatt2003.cardgame.controller;
  opens stud.ntnu.idatt2003.cardgame.controller to javafx.fxml;
}