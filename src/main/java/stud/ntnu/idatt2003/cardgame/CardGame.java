package stud.ntnu.idatt2003.cardgame;

import javafx.application.Application;
import javafx.stage.Stage;
import stud.ntnu.idatt2003.cardgame.controller.CardGameController;

public class CardGame extends Application {
    @Override
    public void start(Stage stage) {
        CardGameController controller = new CardGameController(stage);
    }

    public static void main(String[] args) {
        launch();
    }
}