package stud.ntnu.idatt2003.cardgame.model;

import java.util.List;

public class HandOfCards {
  List<PlayingCard> hand;

  public HandOfCards(List<PlayingCard> hand) {
    this.hand = hand;
  }

  public int getSumOfFaces(HandOfCards hand) {
    return hand.getHand().stream()
            .mapToInt(PlayingCard::getFace)
            .sum();
  }
  public boolean hasFlush(HandOfCards hand) {
    List<PlayingCard> cards = hand.getHand();
    char suit = cards.get(0).getSuit();
    return cards.stream()
            .allMatch(card -> card.getSuit() == suit);
  }
  public boolean hasQueenOfSpades(HandOfCards hand) {
    return hand.getHand().stream()
            .anyMatch(card -> card.getAsString().equals("S12"));
  }
  public String getCardsOfHeartAsString(HandOfCards hand) {
    String hearts = hand.getHand().stream()
            .filter(card -> card.getSuit() == 'H')
            .map(PlayingCard::getAsString)
            .reduce("", (s1, s2) -> s1 + " " + s2);
    if (hearts.isEmpty()) {
      return "No hearts";
    }
    return hearts;
  }
  public List<PlayingCard> getHand() {
    return hand;
  }
}
