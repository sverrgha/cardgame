package stud.ntnu.idatt2003.cardgame.model;

import java.util.*;

public class DeckOfCards {
  private final Map<String, PlayingCard> deck;
  private final char[] suits = new char[]{'S', 'H', 'D', 'C'};

  public DeckOfCards() {
    deck = new HashMap<>();
    for (char suit : suits) {
      for (int face = 1; face <= 13; face++) {
        deck.put("" + suit + face, new PlayingCard(suit, face));
      }
    }
  }

  public List<PlayingCard> dealHand(int numberOfCards) {
    List<PlayingCard> hand = new ArrayList<>();
    Random random = new Random();
    for (int i = 0; i < numberOfCards; i++) {
      int randomSuit = random.nextInt(4);
      hand.add(new PlayingCard(suits[randomSuit], random.nextInt(13) + 1));
    }
    return hand;
  }
}
