package stud.ntnu.idatt2003.cardgame.controller;

import javafx.geometry.Rectangle2D;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import stud.ntnu.idatt2003.cardgame.model.DeckOfCards;
import stud.ntnu.idatt2003.cardgame.model.HandOfCards;
import stud.ntnu.idatt2003.cardgame.view.CardGameView;

public class CardGameController {
  CardGameView view;
  Stage stage;
  VBox root;

  public CardGameController(Stage stage) {
    this.stage = stage;
    this.root = new VBox();
    Rectangle2D primaryScreenBounds = javafx.stage.Screen.getPrimary().getVisualBounds();
    this.view = new CardGameView(this, primaryScreenBounds);
    stage.setScene(view.getScene());
    stage.setTitle("Card Game");
    stage.show();

  }
  public void setRoot(VBox root) {
    this.root = root;
  }

  public VBox getRoot() {
    return root;
  }
  public void dealCards() {
    DeckOfCards deck = new DeckOfCards();
    this.view.setCardContainer(new HandOfCards(deck.dealHand(5)));
  }
  public void checkHand(HandOfCards hand) {
    int sumOfFaces = hand.getSumOfFaces(hand);
    boolean hasFlush = hand.hasFlush(hand);
    boolean hasQueenOfSpades = hand.hasQueenOfSpades(hand);
    String cardsOfHeartAsString = hand.getCardsOfHeartAsString(hand);
    this.view.setTextContainer(sumOfFaces, hasFlush, hasQueenOfSpades, cardsOfHeartAsString);

  }
}